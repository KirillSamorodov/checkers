#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <cstdlib>
#include <conio.h>
#include <windows.h>
#include <wincon.h>


#include <cmath>
using std::abs;

struct Point {
    int y;
    int x;
    Point(int _y, int _x){
        y = _y;
        x = _x;
    }

};

/*
 *                          Правила взяты из игры "Русские шашки" в википедии
 *                          Шашки ходят только по клеткам темного цвета
 *                          Доска расположена так, чтобы угловое поле внизу слева со стороны игрока было темным
 *                          Простая шашка бьет вперед и назад, дамка ходит и бьет на люое поле своей диагонали
 *                          Во время боя простая шашка может превратиться в дамку и сразу продолжить бой по правилам дамки
 *                          При наличии нескольких вариантов боя можно выбрать любой из них
 */

// Функции игры шашки
void gameCheckers();                                                        // Функция для создания игры шашки              Реализовано, автор Кирилл С
bool menuCheckers();                                                        // Меню для шашек                               Реализовано, автор Кирилл С
int** createBoardCheckers();                                                // Создание поля для игры в шашки               Реализовано, автор Кирилл С
void fillBoardCheckers(int** board);                                        // Заполнение поля шашками                      Реализовано, автор Кирилл С
void printBoardCheckers(int** board, Point point, int player, char *str);   // Отображение поля в консоли                   Реализовано, автор Кирилл С
Point moveCheckers(int** board, Point thisPos, int player);                 // Начать движение точки                        Реализовано, автор Кирилл С
Point moveRightPoint(Point point);                                          // Движение направо                             Реализовано, автор Кирилл С
Point moveLeftPoint(Point point);                                           // Движение налево                              Реализовано, автор Кирилл С
Point moveUpPoint(Point point);                                             // Движение вверх                               Реализовано, автор Кирилл С
Point moveDownPoint(Point point);                                           // Движение вних                                Реализовано, автор Кирилл С
bool checkDestroy(int** board, int player);                                 // Проверка, нужно ли рубить шашку              Реализовано, автор Кирилл С
Point startMoving(int** board, Point point, int player);                    // Начать движение шашкой                       Реализовано, автор Кирилл С
Point moveUpLeft(int** board, Point thisPos);                               // Движение вверх налево                        Реализовано, автор Кирилл С
Point moveUpRight(int** board, Point thisPos);                              // Движение вверх направо                       Реализовано, автор Кирилл С
Point moveDownLeft(int** board, Point thisPos);                             // Движение вниз налево                         Реализовано, автор Кирилл С
Point moveDownRight(int** board, Point thisPos);                            // Движение вниз направо                        Реализовано, автор Кирилл С
Point destroy(int** board, Point point, int player);                        // Начать бой шашкой                            Реализовано, автор Кирилл С
bool destroyOnThis(int** board, Point point, int player);                   // Проверка, можно ли начать бой данной шашкой  Реализовано, автор Кирилл С
Point doDestroy(int** board, Point thisPos, int player);                    // Начать бой                                   Реализовано, автор Кирилл С
int winPlayerCheckers(int** board);                                         // Проверка, еть ли победитель                  Реализовано, автор Кирилл С
void printWin(int** board);                                                 // Вывести на экран номер победившего игрока    Реализовано, автор Кирилл С
void checkDamka (int** board);                                              // Проверить появление на доске дамки           Реализовано, автор Кирилл С
Point startMovingDamka(int** board, Point thisPos, int player);             // Реализация движения дамки                    Реализовано, автор Кирилл С
void printDrawCheckers();                                                   // Вывод на экран сообщение о ничье             Реализовано, автор Кирилл С


/*
 * Правила стандартных крестиков ноликов,
 * крестики всегда ходят первыми (это игрок 1)
 */

// Функции игры крестики-нолики
void gameTicTacToe();                                                       // Функция для создания игры крестики нолики    Реализовано, автор Кирилл С
int** createBoardTicTacToe();                                               // Создание поля для игры в крестики нолики     Реализовано, автор Егор
int winPlayerTicTacToe(int** board);                                        // Проверить, нет ли победителя                 Реализовано, автор Кирилл С, Егор
void printBoardTicTacToe(int** board, Point point, int player);             // Вывести на экран доску                       Реализовано, автор Кирилл С, Егор
Point control(int** board, Point point, int player);                        // Реализация выбора позиции                    Реализовано, автор Егор
Point drawCross(int** board, Point point);                                  // Поставить крестик                            Реализовано, автор Егор
Point drawZero(int** board, Point point);                                   // Поставить нолик                              Реализовано, автор Егор
Point moveUp(Point point);                                                  // Движение точки вверх                         Реализовано, автор Егор
Point moveDown(Point point);                                                // Движение точки вниз                          Реализовано, автор Егор
Point moveRight(Point point);                                               // Движение точки направо                       Реализовано, автор Егор
Point moveLeft(Point point);                                                // Движение точки налево                        Реализовано, автор Егор
void printWinner(int player);                                               // Вывод победителя на экран                    Реализовано, автор Кирилл С
void printDraw();                                                           // Вывод ничьи                                  Реализовано, автор Кирилл С
bool checkDraw(int** board);

void chooseGame();                                                          // Выбор игры, воход из игры                    Реализовано, автор Кирилл С

int main() {
    system("chcp 65001");
    chooseGame();
    return 0;
}

void chooseGame() {
    while(1) {
        HANDLE hon = GetStdHandle(STD_OUTPUT_HANDLE);
        COORD coord;
        coord.X = 0;
        coord.Y = 0;

        SetConsoleCursorPosition(hon, coord);

        for (int i = 0; i <= 48; i++){
            for (int j = 0; j <= 60; j++)
                cout << " ";
            cout << endl;
        }

        SetConsoleCursorPosition(hon, coord);

        cout << endl;
        cout << "╔═════════════════╦═════════╗" << endl;
        cout << "║Play tic tac toe ║Press 1  ║" << endl;
        cout << "╚═════════════════╩═════════╝" << endl;
        cout << "╔═════════════════╦═════════╗" << endl;
        cout << "║Play checkers    ║Press 2  ║" << endl;
        cout << "╚═════════════════╩═════════╝" << endl;
        cout << "╔═════════════════╦═════════╗" << endl;
        cout << "║Exit             ║Press 3  ║" << endl;
        cout << "╚═════════════════╩═════════╝" << endl;
        char choice = getch();
        switch (choice) {
            case '1': gameTicTacToe();
            case '2': gameCheckers();
            case '3': return;
        }
    }
}

void gameTicTacToe() {
    int count = 0;
    int player = 1;

    while (menuCheckers() == true) {

        int** board = createBoardTicTacToe();
        Point point(1,1);


        while (winPlayerTicTacToe(board) == 0 && checkDraw(board) == false) {
            printBoardTicTacToe(board, point, player);
            point = control(board, point, player);

            if (player == 1) {
                drawCross(board, point);
                player++;
            }
            else if (player == 2) {
                drawZero(board, point);
                player--;
            }

            count++;
        }
        printWinner(winPlayerTicTacToe(board));
    }



}

void printWinner(int player) {
    HANDLE hon = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coord;
    coord.X = 0;
    coord.Y = 0;

    SetConsoleCursorPosition(hon, coord);

    for (int i = 0; i <= 48; i++){
        for (int j = 0; j <= 60; j++)
            cout << " ";
        cout << endl;
    }

    SetConsoleCursorPosition(hon, coord);

    if (player != 0) {
        cout << "╔════════════════════════════╗" << endl;
        cout << "║Player "<< player <<" WIN!!!             ║" << endl;
        cout << "╚════════════════════════════╝" << endl;
        getch();


        SetConsoleCursorPosition(hon, coord);

        for (int i = 0; i <= 48; i++){
            for (int j = 0; j <= 60; j++)
                cout << " ";
            cout << endl;
        }

        SetConsoleCursorPosition(hon, coord);

    }
    else printDraw();
}

void printDraw() {
    cout << "╔════════════════╗" << endl;
    cout << "║   Draw :(      ║" << endl;
    cout << "╚════════════════╝" << endl;
    getch();

    HANDLE hon = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coord;
    coord.X = 0;
    coord.Y = 0;
    SetConsoleCursorPosition(hon, coord);

    for (int i = 0; i <= 48; i++){
        for (int j = 0; j <= 60; j++)
            cout << " ";
        cout << endl;
    }

    SetConsoleCursorPosition(hon, coord);
}

bool checkDraw(int** board) {
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            if (board[i][j] == 0) return false;
    return true;
}

void printBoardTicTacToe(int** board, Point point, int player){
    HANDLE hon = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coord;
    coord.X = 0;
    coord.Y = 0;
    SetConsoleCursorPosition(hon, coord);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; ++j) {
            if (point.x != j || point.y != i)
                cout << "┌───┐";
            else cout << "     ";
        }

        cout << endl;
        for (int j = 0; j < 3; ++j) {
            if (point.x != j || point.y != i)
                switch (board[i][j]) {
                    case 0: cout << "|   |"; break;
                    case 1: cout << "│ X │"; break;
                    case 2: cout << "│ 0 │"; break;
                }
            else
                switch (board[i][j]) {
                    case 0: cout << "     "; break;
                    case 1: cout << "  X  "; break;
                    case 2: cout << "  0  "; break;
                }
        }

        cout << endl;
        for (int j = 0; j < 3; ++j) {
            if (point.x != j || point.y != i)
                cout << "└───┘";
            else cout << "     ";
        }
        cout << endl;
    }

}

int** createBoardTicTacToe() {
    int** board = new int*[3];

    for (int i = 0; i < 3; ++i) {
        board[i] = new int[3];
    }
    // Заполнить матрицу нулями
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            board[i][j] = 0;
        }
    }
    return board;
}

int winPlayerTicTacToe(int** board){
    int player = 0;
    for (int k = 0; k < 3; ++k) {
        for (int i = 0; i < 3; ++i) {
            if (board[k][i] == board[k][0])
                player = board[k][0];
            else {
                player = 0;
                break;
            }
        }

        if (player != 0)
            return player;

        for (int i = 1; i < 3; ++i) {
            if (board[i][k] == board[0][k])
                player = board[0][k];
            else {
                player = 0;
                break;
            }
        }
        if (player != 0)
            return player;
    }

    if (board[0][0] != 0) {

        if (board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
            player = board[0][0];
            return player;
        }

        if (board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
            player = board[0][2];
            return player;
        }
    }

    return player;

}

Point control(int** board, Point thisPos, int player) {
    char choice;
    bool check = true;
    do {
        check = true;
        choice = getch();
        switch (choice) {
            case 'w':
                thisPos = moveUp(thisPos);
                break;
            case 'a':
                thisPos = moveLeft(thisPos);
                break;
            case 's':
                thisPos = moveDown(thisPos);
                break;
            case 'd':
                thisPos = moveRight(thisPos);
                break;
            case 13:
                if (board[thisPos.y][thisPos.x] != 0) check = false;
                if (board[thisPos.y][thisPos.x] == 0 && player == 1)
                    drawCross(board, thisPos);
                if (board[thisPos.y][thisPos.x] == 0 && player == 2)
                    drawZero(board, thisPos);

                break;
            default:
                break;
        }
        printBoardTicTacToe(board, thisPos, player);
    } while (choice != '\r' || check == false);

    return thisPos;
}

Point moveUp(Point point) {
    if (point.y > 0)
        point.y--;
    return point;
}

Point moveDown(Point point) {
    if (point.y < 2)
        point.y++;
    return point;
}

Point moveLeft(Point point) {
    if (point.x > 0)
        point.x--;
    return point;
}

Point moveRight(Point point) {
    if (point.x < 2)
        point.x++;
    return point;
}

Point drawCross(int** board, Point point){
    if (board[point.y][point.x] == 0)
        board[point.y][point.x] = 1;
    return point;
}

Point drawZero(int** board, Point point){
    if (board[point.y][point.x] == 0)
        board[point.y][point.x] = 2;
    return point;
}

void gameCheckers() {
    while (menuCheckers() == true){
        int** board = createBoardCheckers();
        Point point(4,4);
        int player = 1;
        int count = 0;
        while (winPlayerCheckers(board) == 0 && count != 30) {
            printBoardCheckers(board, point, player, "Move                ");
            if (checkDestroy(board, player))
                while (checkDestroy(board, player)){
                    point = destroy(board, point, player);
                    checkDamka(board);
                    count = 0;
                }
            else {
                count++;
                point = moveCheckers(board, point, player);
            }

            checkDamka(board);

            if (player == 1) player = 2;
            else player = 1;

        }
        if (count != 30){
            printWin(board);
        }
        else
            printDrawCheckers();
        for (int i = 0; i < 8; ++i) {
            delete[] board[i];
        }
        delete[] board;
    }
}

bool menuCheckers() {
    HANDLE hon = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coord;
    coord.X = 0;
    coord.Y = 0;

    SetConsoleCursorPosition(hon, coord);

    for (int i = 0; i <= 48; i++){
        for (int j = 0; j <= 60; j++)
            cout << " ";
        cout << endl;
    }

    SetConsoleCursorPosition(hon, coord);

    cout << endl;
    cout << "╔════════════╦═══════════╗" << endl;
    cout << "║    Start   ║  press 1  ║" << endl;
    cout << "╚════════════╩═══════════╝" << endl;
    cout << "╔════════════╦═══════════╗" << endl;
    cout << "║    Exit    ║  press 2  ║" << endl;
    cout << "╚════════════╩═══════════╝" << endl;

    char choice = getch();

    SetConsoleCursorPosition(hon, coord);

    for (int i = 0; i <= 48; i++){
        for (int j = 0; j <= 60; j++)
            cout << " ";
        cout << endl;
    }

    SetConsoleCursorPosition(hon, coord);

    if (choice == '1'){
        return true;
    }
    else chooseGame();
}

int** createBoardCheckers() {
    int** board = new int*[8];

    for (int i = 0; i < 8; ++i) {
        board[i] = new int[8];
    }

    fillBoardCheckers(board);

    return board;
}

void fillBoardCheckers(int** board) {
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            if ((i + j) % 2 == 0) board[i][j] = 0;
            else if (i < 3)
                board[i][j] = 2;
            else if (i >= 5)
                board[i][j] = 1;
            else board[i][j] = 0;
        }
    }
}

void printBoardCheckers(int** board, Point point, int player, char *str){
    HANDLE hon = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coord;
    coord.X = 0;
    coord.Y = 0;
    SetConsoleCursorPosition(hon, coord);
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (i != point.y || j != point.x)
                cout << "┌───┐";
            else cout << "     ";
        }
        cout << endl;

        for (int j = 0; j < 8; ++j) {
            if (i != point.y || j != point.x) {
                if (board[i][j] == 1)
                    cout << "│ " << "█" << " │";
                else if (board[i][j] == 2)
                    cout << "│ " << "▒" << " │";
                else if (board[i][j] == 3){
                    cout << "│ " << "◙" << " │";
                }
                else if (board[i][j] == 4){
                    cout << "│ " << "▼" << " │";
                }
                else cout << "│   │";
            }
            else {
                if (board[i][j] == 1)
                    cout << "  " << "█" << "  ";
                else if (board[i][j] == 2)
                    cout << "  " << "▒" << "  ";
                else if (board[i][j] == 3){
                    cout << "│ " << "◙" << " │";
                }
                else if (board[i][j] == 4){
                    cout << "│ " << "▼" << " │";
                }
                else cout << "     ";
            }
        }
        cout << endl;
        for (int j = 0; j < 8; ++j) {
            if (i != point.y || j != point.x) {
                cout << "└───┘";
            } else cout << "     ";
        }
        cout << endl;

    }

    cout << endl;

    if (player > 2) player -= 2;
    cout << str << endl << player << " player" << endl;
}

Point moveCheckers(int** board, Point thisPos, int player) {
    HANDLE hon = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coord;
    coord.X = 0;
    coord.Y = 0;

    bool startMove = false;
    bool startMoveDamka = false;
    do {
        char choice = getch();
        switch (choice) {
            case 'w':
                thisPos = moveUpPoint(thisPos);
                break;
            case 'a':
                thisPos = moveLeftPoint(thisPos);
                break;
            case 's':
                thisPos = moveDownPoint(thisPos);
                break;
            case 'd':
                thisPos = moveRightPoint(thisPos);
                break;
            case 13:
                if (board[thisPos.y][thisPos.x] == player)
                    startMove = true;
                if (board[thisPos.y][thisPos.x] == player + 2)
                    startMoveDamka = true;
                break;
            case 27: {

                SetConsoleCursorPosition(hon, coord);

                for (int i = 0; i <= 48; i++){
                    for (int j = 0; j <= 60; j++)
                        cout << " ";
                    cout << endl;
                }

                SetConsoleCursorPosition(hon, coord);

                cout << endl;
                cout << "╔════════════╦═══════════╗" << endl;
                cout << "║ Continue   ║  press 1  ║" << endl;
                cout << "╚════════════╩═══════════╝" << endl;
                cout << "╔════════════╦═══════════╗" << endl;
                cout << "║ Exit       ║  press 2  ║" << endl;
                cout << "╚════════════╩═══════════╝" << endl;

                char ch = getch();
                SetConsoleCursorPosition(hon, coord);

                if (ch == '2') {
                    for (int i = 0; i < 8; ++i) {
                        delete[] board[i];
                    }
                    delete[] board;
                    main();
                }

                break;
            }
            default: break;
        }
        printBoardCheckers(board, thisPos, player, "Choose figure       ");
    } while (startMove == false && startMoveDamka == false);
    if (startMove)
        thisPos = startMoving(board, thisPos, player);//////Проверить, работает ли
    else if (startMoveDamka){
        thisPos = startMovingDamka(board, thisPos, player);
    }
    return thisPos;

}

Point moveRightPoint(Point point) {
    if (point.x + 1 < 8)
        point.x++;
    return point;
}

Point moveLeftPoint(Point point) {
    if (point.x - 1 >= 0)
        point.x--;
    return point;
}

Point moveUpPoint(Point point) {
    if (point.y - 1 >= 0)
        point.y--;
    return point;
}

Point moveDownPoint(Point point) {
    if (point.y + 1 < 8)
        point.y++;
    return point;
}

bool checkDestroy(int** board, int player) {
    bool check = false;
    int enemy;
    int k;
    int l;

    if (player == 1){
        enemy = 2;
    }
    else enemy = 1;
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (board[i][j] == player) {
                if (i - 1 >= 0 && j - 1 >= 0)
                    if (board[i - 1][j - 1] != player && board[i - 1][j - 1] != player + 2 && board[i - 1][j - 1] != 0)
                        if (i - 2 >= 0 && j - 2 >= 0)
                            if (board[i - 2][j - 2] == 0)
                                check = true;
                if (i - 1 >= 0 && j + 1 < 8)
                    if (board[i - 1][j + 1] != player && board[i - 1][j + 1] != player + 2  && board[i - 1][j + 1] != 0)
                        if (i - 2 >= 0 && i - 2 < 8 && j + 2 >= 0 && j + 2 < 8)
                            if (board[i - 2][j + 2] == 0)
                                check = true;

                if (i + 1 < 8 && j - 1 >= 0)
                    if (board[i + 1][j - 1] != player && board[i + 1][j - 1] != player + 2  && board[i + 1][j - 1] != 0)
                        if (i + 2 < 8 && j - 2 >= 0)
                            if (board[i + 2][j - 2] == 0)
                                check = true;

                if (i + 1 < 8 && j + 1 < 8)
                    if (board[i + 1][j + 1] != player && board[i + 1][j + 1] != player + 2  && board[i + 1][j + 1] != 0)
                        if (i + 2 < 8 && j + 2 < 8)
                            if (board[i + 2][j + 2] == 0 && i + 2 < 8 && j + 2 < 8)
                                check = true;

                ///проверка на возможность дествоя дамкой/////////
            } else if (board[i][j] == player + 2) {
                k = i;
                l = j;
                while (board[k][l] != enemy && board[k][l] != enemy + 2 && k != 7 && l != 7 && check == false){
                    k++;
                    l++;
                    if ((board[k][l] == enemy || board[k][l] == enemy + 2) && k + 1 <= 7 && l + 1 <= 7)
                        if (board[k + 1][l + 1] == 0 && (board[k][l] == enemy || board[k][l] == enemy + 2))
                            check = true;
                }
                k = i;
                l = j;
                while (board[k][l] != enemy && board[k][l] != enemy + 2 && l != 0 && k != 7 && check == false){
                    k++;
                    l--;
                    if ((board[k][l] == enemy || board[k][l] == enemy + 2) && k + 1 <= 7 && l - 1 >= 0)
                        if (board[k + 1][l - 1] == 0 && (board[k][l] == enemy || board[k][l] == enemy + 2))
                            check = true;
                }
                k = i;
                l = j;
                while (board[k][l] != enemy && board[k][l] != enemy + 2 && k != 0 && l != 0 && check == false){
                    k--;
                    l--;
                    if ((board[k][l] == enemy || board[k][l] == enemy + 2) && k - 1 >= 0 && l - 1 >= 0)
                        if (board[k - 1][l - 1] == 0 && (board[k][l] == enemy || board[k][l] == enemy + 2))
                            check = true;
                }
                k = i;
                l = j;
                while (board[k][l] != enemy && board[k][l] != enemy + 2 && k != 0 && l != 7 && check == false){
                    k--;
                    l++;
                    if ((board[k][l] == enemy || board[k][l] == enemy + 2) && k - 1 >= 0 && l + 1 <= 7)
                        if (board[k - 1][l + 1] == 0 && (board[k][l] == enemy || board[k][l] == enemy + 2))
                            check = true;
                }
            }
        }
    }
    return check;
}

Point startMoving(int** board, Point thisPos, int player){
    Point nextPos = thisPos;
    bool moving = false;
    do {
        char choice = getch();
        switch (choice) {
            case 'w':
                nextPos = moveUpPoint(nextPos);
                break;
            case 'a':
                nextPos = moveLeftPoint(nextPos);
                break;
            case 's':
                nextPos = moveDownPoint(nextPos);
                break;
            case 'd':
                nextPos = moveRightPoint(nextPos);
                break;
            case 13:
                if (thisPos.x - 1 == nextPos.x && thisPos.y - 1 == nextPos.y && player == 1){
                    moving = true;
                    nextPos = moveUpLeft(board, thisPos);
                }
                if (thisPos.x + 1 == nextPos.x && thisPos.y - 1 == nextPos.y && player == 1){
                    moving  = true;
                    nextPos = moveUpRight(board, thisPos);
                }
                if (thisPos.x - 1 == nextPos.x && thisPos.y + 1 == nextPos.y && player == 2){
                    moving  = true;
                    nextPos = moveDownLeft(board, thisPos);
                }
                if (thisPos.x + 1 == nextPos.x && thisPos.y + 1 == nextPos.y && player == 2){
                    moving  = true;
                    nextPos = moveDownRight(board, thisPos);
                }
                break;
            default: break;
        }
        printBoardCheckers(board, nextPos, player, "Move                ");
    } while (moving == false);

    return nextPos;
}


Point moveUpLeft(int** board, Point thisPos) {

    if (thisPos.y - 1 >= 0 && thisPos.x - 1 >= 0){
        if (board[thisPos.y - 1][thisPos.x - 1] == 0){
            board[thisPos.y - 1][thisPos.x - 1] = board[thisPos.y][thisPos.x];
            board[thisPos.y][thisPos.x] = 0;
            thisPos.y--;
            thisPos.x--;
        }
    }

    return thisPos;
}

Point moveUpRight(int** board, Point thisPos) {

    if (thisPos.y - 1 >= 0 && thisPos.x + 1 < 8){
        if (board[thisPos.y - 1][thisPos.x + 1] == 0){
            board[thisPos.y - 1][thisPos.x + 1] = board[thisPos.y][thisPos.x];
            board[thisPos.y][thisPos.x] = 0;
            thisPos.y--;
            thisPos.x++;
        }
    }

    return thisPos;
}

Point moveDownRight(int** board, Point thisPos) {

    if (thisPos.y + 1 < 8 && thisPos.x + 1 < 8){
        if (board[thisPos.y + 1][thisPos.x + 1] == 0){
            board[thisPos.y + 1][thisPos.x + 1] = board[thisPos.y][thisPos.x];
            board[thisPos.y][thisPos.x] = 0;
            thisPos.y++;
            thisPos.x++;
        }
    }

    return thisPos;
}

Point moveDownLeft(int** board, Point thisPos) {

    if (thisPos.y + 1 < 8 && thisPos.x - 1 >= 0){
        if (board[thisPos.y + 1][thisPos.x - 1] == 0){
            board[thisPos.y + 1][thisPos.x - 1] = board[thisPos.y][thisPos.x];
            board[thisPos.y][thisPos.x] = 0;
            thisPos.y++;
            thisPos.x--;
        }
    }

    return thisPos;
}

Point destroy(int** board, Point thisPos, int player) {
    Point nextPos = thisPos;
    bool move;
    do {
        move = false;
        char choice = getch();
        switch (choice) {
            case 'w':
                nextPos = moveUpPoint(nextPos);
                break;
            case 'a':
                nextPos = moveLeftPoint(nextPos);
                break;
            case 's':
                nextPos = moveDownPoint(nextPos);
                break;
            case 'd':
                nextPos = moveRightPoint(nextPos);
                break;
            case 13:
                if (board[nextPos.y][nextPos.x] != 0)
                    move = true;
                break;
            default: break;
        }
        printBoardCheckers(board, nextPos, player, "Move to destroy     ");
    } while (destroyOnThis(board, nextPos, player) == false || move == false);
    if (board[nextPos.y][nextPos.x] > 2)
        player += 2;
    nextPos = doDestroy(board, nextPos, player);
    return nextPos;
}

bool destroyOnThis(int** board, Point point, int player) {
    int i = point.y;
    int j = point.x;
    bool check = false;
    int k = 0;
    int l = 0;
    int enemy;
    if (player == 1 || player == 3)
        enemy = 2;
    else enemy = 1;
    if (i - 1 >= 0 && j - 1 >= 0)
        if (board[i - 1][j - 1] != player && board[i - 1][j - 1] != player + 2 && board[i - 1][j - 1] != 0)
            if (i - 2 >= 0 && j - 2 >= 0)
                if (board[i - 2][j - 2] == 0)
                    check = true;
    if (i - 1 >= 0 && j + 1 < 8)
        if (board[i - 1][j + 1] != player && board[i - 1][j + 1] != player + 2  && board[i - 1][j + 1] != 0)
            if (i - 2 >= 0 && i - 2 < 8 && j + 2 >= 0 && j + 2 < 8)
                if (board[i - 2][j + 2] == 0)
                    check = true;

    if (i + 1 < 8 && j - 1 >= 0)
        if (board[i + 1][j - 1] != player && board[i + 1][j - 1] != player + 2  && board[i + 1][j - 1] != 0)
            if (i + 2 < 8 && j - 2 >= 0)
                if (board[i + 2][j - 2] == 0)
                    check = true;

    if (i + 1 < 8 && j + 1 < 8)
        if (board[i + 1][j + 1] != player && board[i + 1][j + 1] != player + 2  && board[i + 1][j + 1] != 0)
            if (i + 2 < 8 && j + 2 < 8)
                if (board[i + 2][j + 2] == 0 && i + 2 < 8 && j + 2 < 8)
                    check = true;

    if (board[i][j] == player + 2) {
        k = i;
        l = j;
        while (board[k][l] != enemy && board[k][l] != enemy + 2 && k != 7 && l != 7 && check == false){
            k++;
            l++;
            if ((board[k][l] == enemy || board[k][l] == enemy + 2) && k + 1 <= 7 && l + 1 <= 7)
                if (board[k + 1][l + 1] == 0 && (board[k][l] == enemy || board[k][l] == enemy + 2))
                    check = true;
        }
        k = i;
        l = j;
        while (board[k][l] != enemy && board[k][l] != enemy + 2 && l != 0 && k != 7 && check == false){
            k++;
            l--;
            if ((board[k][l] == enemy || board[k][l] == enemy + 2) && k + 1 <= 7 && l - 1 >= 0)
                if (board[k + 1][l - 1] == 0 && (board[k][l] == enemy || board[k][l] == enemy + 2))
                    check = true;
        }
        k = i;
        l = j;
        while (board[k][l] != enemy && board[k][l] != enemy + 2 && k != 0 && l != 0 && check == false){
            k--;
            l--;
            if ((board[k][l] == enemy || board[k][l] == enemy + 2) && k - 1 >= 0 && l - 1 >= 0)
                if (board[k - 1][l - 1] == 0 && (board[k][l] == enemy || board[k][l] == enemy + 2))
                    check = true;
        }
        k = i;
        l = j;
        while (board[k][l] != enemy && board[k][l] != enemy + 2 && k != 0 && l != 7 && check == false){
            k--;
            l++;
            if ((board[k][l] == enemy || board[k][l] == enemy + 2) && k - 1 >= 0 && l + 1 <= 7)
                if (board[k - 1][l + 1] == 0 && (board[k][l] == enemy || board[k][l] == enemy + 2))
                    check = true;
        }
    }


    return check;
}

Point doDestroy(int** board, Point thisPos, int player) {
    Point nextPos = thisPos;
    bool move;
    int deltaX = 0;
    int deltaY = 0;
    // int nextX;
    // int nextY;
    do {
        move = false;
        char choice = getch();
        switch (choice) {
            case 'w':
                nextPos = moveUpPoint(nextPos);
                break;
            case 'a':
                nextPos = moveLeftPoint(nextPos);
                break;
            case 's':
                nextPos = moveDownPoint(nextPos);
                break;
            case 'd':
                nextPos = moveRightPoint(nextPos);
                break;
            case 13:
                if (player <= 2) {
                    if (nextPos.x == thisPos.x + 2 && nextPos.y == thisPos.y + 2) {
                        if (board[thisPos.y + 1][thisPos.x + 1] != 0 && board[thisPos.y + 1][thisPos.x + 1] != player && board[thisPos.y + 1][thisPos.x + 1] != player + 2) {
                            move = true;
                            board[thisPos.y + 1][thisPos.x + 1] = 0;
                            board[thisPos.y][thisPos.x] = 0;
                            board[nextPos.y][nextPos.x] = player;
                        }
                    } else if (nextPos.x == thisPos.x - 2 && nextPos.y == thisPos.y + 2) {
                        if (board[thisPos.y + 1][thisPos.x - 1] != 0 && board[thisPos.y + 1][thisPos.x - 1] != player && board[thisPos.y + 1][thisPos.x - 1] != player + 2) {
                            move = true;
                            board[thisPos.y + 1][thisPos.x - 1] = 0;
                            board[thisPos.y][thisPos.x] = 0;
                            board[nextPos.y][nextPos.x] = player;
                        }
                    } else if (nextPos.x == thisPos.x + 2 && nextPos.y == thisPos.y - 2) {
                        if (board[thisPos.y - 1][thisPos.x + 1] != 0 && board[thisPos.y - 1][thisPos.x + 1] != player && board[thisPos.y - 1][thisPos.x + 1] != player + 2) {
                            move = true;
                            board[thisPos.y - 1][thisPos.x + 1] = 0;
                            board[thisPos.y][thisPos.x] = 0;
                            board[nextPos.y][nextPos.x] = player;
                        }
                    } else if (nextPos.x == thisPos.x - 2 && nextPos.y == thisPos.y - 2) {
                        if (board[thisPos.y - 1][thisPos.x - 1] != 0 && board[thisPos.y - 1][thisPos.x - 1] != player && board[thisPos.y - 1][thisPos.x - 1] != player + 2) {
                            move = true;
                            board[thisPos.y - 1][thisPos.x - 1] = 0;
                            board[thisPos.y][thisPos.x] = 0;
                            board[nextPos.y][nextPos.x] = player;
                        }
                    }


                    //// Реализовать дестрой дамкой
                }
                else {
                    if (board[nextPos.y][nextPos.x] == 0) {
                        deltaX = nextPos.x - thisPos.x;
                        deltaY = nextPos.y - thisPos.y;
                        int enemy;
                        if (player == 3) enemy = 2;
                        else enemy = 1;
                        if (abs(deltaX) == abs(deltaY) && nextPos.x - thisPos.x != 0) {
                            deltaX /= abs(nextPos.x - thisPos.x);
                            deltaY /= abs(nextPos.y - thisPos.y);
                            int k = thisPos.y + deltaY;
                            int l = thisPos.x + deltaX;
                            while (board[k][l] == 0) {
                                k += deltaY;
                                l += deltaX;
                            }
                            if (board[k][l] == enemy || board[k][l] == enemy + 2) {
                                int a = k;
                                int b = l;
                                while ((k != nextPos.y || l != nextPos.x) && k > -1 && k < 8 && l > -1 && l < 8 && board[k + deltaY][l + deltaX] == 0) {
                                    k += deltaY;
                                    l += deltaX;
                                }
                                if (k == nextPos.y && l == nextPos.x && a != k && b != l && board[k][l] == 0) {
                                    board[k][l] = player;
                                    board[thisPos.y][thisPos.x] = 0;
                                    board[a][b] = 0;

                                    move = true;
                                }
                            } else break;
                        }
                    }
                }

                break;
            default: break;
        }

        printBoardCheckers(board, nextPos, player, "Destroy             ");
    } while (move == false);

    return nextPos;

}

int winPlayerCheckers(int** board){
    bool player1 = true;
    bool player2 = true;
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (board[i][j] == 1) player2 = false;
            if (board[i][j] == 2) player1 = false;
        }
    }
    if (player1 == true)
        return 1;
    else if (player2 == true)
        return 2;
    else return 0;
}

void printWin(int** board){
    cout << "╔═══════════════════════════════════╗" << endl;
    cout << "║Player " << winPlayerCheckers(board) << " win!!! Press 1 to continue║" << endl;
    cout << "╚═══════════════════════════════════╝" << endl;
    char next = getch();
}

void checkDamka (int** board) {

    for (int j = 0; j < 8; ++j) {
        if (board[0][j] == 1)
            board[0][j] += 2;
        if (board[7][j] == 2)
            board[7][j] += 2;
    }
}

Point startMovingDamka(int** board, Point thisPos, int player){
    Point nextPos = thisPos;
    bool moving = false;
    int deltaX;
    int deltaY;
    do {
        char choice = getch();
        switch (choice) {
            case 'w':
                nextPos = moveUpPoint(nextPos);
                break;
            case 'a':
                nextPos = moveLeftPoint(nextPos);
                break;
            case 's':
                nextPos = moveDownPoint(nextPos);
                break;
            case 'd':
                nextPos = moveRightPoint(nextPos);
                break;
            case 13:
                if (board[nextPos.y][nextPos.x] == 0) {
                    deltaX = nextPos.x - thisPos.x;
                    deltaY = nextPos.y - thisPos.y;
                    if (abs(deltaX) == abs(deltaY)) {
                        deltaX /= abs(nextPos.x - thisPos.x);
                        deltaY /= abs(nextPos.y - thisPos.y);
                        int k = thisPos.y + deltaY;
                        int l = thisPos.x + deltaX;
                        while (k != nextPos.y && l != nextPos.x && k <= 7 && k >= 0 && l <= 7 && l >= 0 && board[k][l] == 0) {
                            k += deltaY;
                            l += deltaX;
                        }

                        if (k == nextPos.y && l == nextPos.x) {
                            moving = true;
                            board[thisPos.y][thisPos.x] = 0;
                            board[nextPos.y][nextPos.x] = player + 2;
                        }
                    }
                    break;
                    default:
                        break;
                }
        }

        printBoardCheckers(board, nextPos, player, "Move damka          ");
    } while (moving == false);

    return nextPos;
}

void printDrawCheckers(){
    cout << "╔═══════════════════════════════════╗" << endl;
    cout << "║Draw, 30 empty moveCheckers:(      ║" << endl;
    cout << "╚═══════════════════════════════════╝" << endl;
    char next = getch();
}

